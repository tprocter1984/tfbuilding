<?php

/**
 * Include the Custom Load of the Theme.
 */
require_once dirname( __FILE__ ) . '/content/functions/load.php';
require_once dirname( __FILE__ ) . '/inc/templates.php';
require_once dirname( __FILE__ ) . '/news/news.php';
require_once dirname( __FILE__ ) . '/ihaf/ihaf.php';